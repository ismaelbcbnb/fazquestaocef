import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-classes',
  templateUrl: './list-classes.component.html',
  styleUrls: ['./list-classes.component.css']
})
export class ListClassesComponent implements OnInit {

  listaMaterias= [
    {id: 1, nome:  '1 - PLATAFORMA - COMO USAR'},
    {id: 2, nome:  '2 - EDITAL - MAPA da PROVA e PLANO de ESTUDOS'},
    {id: 3, nome:  '3 - TRILHAS - CAIXA ECONÔMICA - TÉCNICO BANCÁRIO'},
    {id: 4, nome:  '4 - PORTUGUÊS'},
    {id: 5, nome:  '5 - QUESTÕES PORTUGUÊS - CESGRANRIO'},
    {id: 6, nome:  '6 - LÍNGUA INGLESA e QUESTÕES'},
    {id: 7, nome:  '7 - MATEMÁTICA FINANCEIRA'},
    {id: 8, nome:  '8 - QUESTÕES MATEMÁTICA FINANCEIRA - CESGRANRIO'},
    {id: 9, nome:  '9 - NOÇÕES de PROBABILIDADE e ESTATÍSTICA'},
    {id: 10, nome: '10 - QUESTÕES PROBABILIDADE e ESTATÍSTICA - CESGRANRIO'},
    {id: 11, nome: '11 - COMPORTAMENTO ÉTICO, COMPLIANCE e QUESTÕES'},
    {id: 12, nome: '12 - CONHECIMENTOS BANCÁRIOS'},
    {id: 13, nome: '13 - QUESTÕES CONHECIMENTOS BANCÁRIOS - CESGRANRIO'},
    {id: 14, nome: '14 - ATUALIDADES DO MERCADO FINANCEIRO'},
    {id: 15, nome: '15 - QUESTÕES ATUALIDADES DO MERCADO FINANCEIRO'},
    {id: 16, nome: '16 - INFORMÁTICA'},
    {id: 17, nome: '17 - QUESTÕES INFORMÁTICA - CESGRANRIO'},
    {id: 18, nome: '18 - CONHECIMENTOS, COMPORTAMENTOS DIGITAIS e QUESTÕES'},
    {id: 19, nome: '19 - ATENDIMENTO - VENDAS e NEGOCIAÇÃO'},
    {id: 20, nome: '20 - QUESTÕES ATENDIMENTO - CESGRANRIO'},
    {id: 21, nome: '21 - REDAÇÃO'},
  ]
  
  
  
  

  constructor() { }

  ngOnInit(): void {
  }

}
